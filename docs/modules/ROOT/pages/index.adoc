= README
:author:            WOLfgang Schricker
:email:             time@wols.org
include::ROOT:partial$variables.adoc[]
:page-toclevels:    -1
// NO empty line before!

image::cover.png[NTPstats-NG]

include::partial$readme.adoc[leveloffset=0]

//

include::partial$footer.adoc[]

// ntpstatsng/antora/modules/ROOT/pages/index.adoc

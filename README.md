# README

## NTP statistics - Next Generation

https://ntpstatsng.gitlab.io/ntpstatsng/ntpstatsng/main/

**IMPORTANT**

    This software is under the heavy development and considered ALPHA quality till the version hits >=1.0.0.
    Things might be broken, not all features have been implemented and APIs will be likely to change.

    YOU HAVE BEEN WARNED.

![screenshot](docs/modules/ROOT/images/cover.png "screenshot")

## License

This program is free software: you can redistribute it and/or modify it under the terms of the **GNU General Public License** as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a [copy](LICENSE.md) of the GNU General Public License along with this program.
If not, see https://www.gnu.org/licenses/.

***

NTPstats-NG © 2017-2023 WOLfgang Schricker
